<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(Profile)                              _c620fd</name>
   <tag></tag>
   <elementGuidId>57297f89-9fde-44d8-9ce9-9e89368fa3e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-12</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)173070101****
            รหัสนิสิต (Student code)
          62160303
        
        
          ชื่อ - นามสกุล (Name)
          Saksit Waenwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-17 16:32:52 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:38
        
    

 

 


			 
	

                    
                    
                </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)173070101****
            รหัสนิสิต (Student code)
          62160303
        
        
          ชื่อ - นามสกุล (Name)
          Saksit Waenwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-17 16:32:52 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:38
        
    

 

 


			 
	

                    
                    
                ' or . = '
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)173070101****
            รหัสนิสิต (Student code)
          62160303
        
        
          ชื่อ - นามสกุล (Name)
          Saksit Waenwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-17 16:32:52 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:38
        
    

 

 


			 
	

                    
                    
                ')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)173070101****
            รหัสนิสิต (Student code)
          62160303
        
        
          ชื่อ - นามสกุล (Name)
          Saksit Waenwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-07-13 06:34:42 (balance : 113 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:38
        
    

 

 


			 
	

                    
                    
                ' or . = '
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)173070101****
            รหัสนิสิต (Student code)
          62160303
        
        
          ชื่อ - นามสกุล (Name)
          Saksit Waenwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-07-13 06:34:42 (balance : 113 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:38
        
    

 

 


			 
	

                    
                    
                ')]</value>
   </webElementXpaths>
</WebElementEntity>
